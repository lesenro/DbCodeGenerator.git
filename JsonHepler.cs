﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;

namespace DbCodeGenerator
{
    class JsonHepler
    {
        public static string ToJson<T>(T t)
        {
            using (var ms = new MemoryStream())
            {
                new DataContractJsonSerializer(t.GetType()).WriteObject(ms, t);
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }
        public static T JsonParse<T>(string jsonString)
        {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)))
            {
                return (T)new DataContractJsonSerializer(typeof(T)).ReadObject(ms);
            }
        }
    }
}
