﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DbCodeGenerator
{
    public interface IDbHandle
    {
        string ServerAddress { get; set; }
        string UserID { get; set; }
        string Password { get; set; }
        uint Port { get; set; }
        IDbConnection getDbConnection(string dbNmae);
        IDbConnection getDbConnection();
        DataTable getDatabases();
        DataTable getTables(string dbname);
        DataTable getColumns(string dbname, string tabname);
    }
}
