﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace DbCodeGenerator
{
    public partial class DCGForm : Form
    {
        private IDbHandle dbHandle;
        public DCGForm()
        {
            InitializeComponent();
            dbtypeBind();
            nameStyleBind();
        }
        private void dbtypeBind()
        {
            Dictionary<string, string> dbtype;
            dbtype = new Dictionary<string, string>();
            dbtype.Add("", "请选择数据库类型");
            dbtype.Add("mysql", "MySql");
            dbtype.Add("mssql", "Sql Server");
            BindingSource bs = new BindingSource();
            bs.DataSource = dbtype;
            ccbDbType.DataSource = bs;
            ccbDbType.DisplayMember = "Value";
            ccbDbType.ValueMember = "Key";
        }

        private void DCGForm_Load(object sender, EventArgs e)
        {


        }
        private void dbInit()
        {
            string dbkey = ccbDbType.SelectedValue.ToString();
            switch (dbkey)
            {
                case "mysql":
                    dbHandle = new MysqlDbHandle
                    {
                        ServerAddress = "127.0.0.1",
                        UserID = "root",
                        Password = "",
                        Port = 3306
                    };
                    break;
                case "mssql":
                    dbHandle = new SqlServerDbHandle
                    {
                        ServerAddress = "127.0.0.1",
                        UserID = "sa",
                        Password = "",
                        Port=1433
                    };
                    break;
                default:
                    return;
            }
            txtPassword.Text = dbHandle.Password;
            txtServerAddress.Text = dbHandle.ServerAddress;
            txtUserId.Text = dbHandle.UserID;
            numPort.Value = dbHandle.Port;
        }
        private void dbConn()
        {
            dbHandle.ServerAddress = txtServerAddress.Text;
            dbHandle.Password = txtPassword.Text;
            dbHandle.UserID = txtUserId.Text;
            dbHandle.Port = (uint)numPort.Value;
            try {
                DataTable dbs = dbHandle.getDatabases();
                ccbDataBase.DisplayMember = "Database_Name";
                ccbDataBase.ValueMember = "Database_Name";
                ccbDataBase.DataSource = dbs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "连接错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void btnDbConn_Click(object sender, EventArgs e)
        {
            if (dbHandle == null)
            {
                return;
            }
            dbConn();
        }

        private void ccbDbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dbInit();
        }

        private void ccbDataBase_SelectedIndexChanged(object sender, EventArgs e)
        {
            tabList.Items.Clear();
            if (dbHandle == null)
            {
                return;
            }
            string dbname = ccbDataBase.SelectedValue.ToString();
            if (dbname == "")
            {
                return;
            }
            DataTable tabs = dbHandle.getTables(dbname);
            foreach (DataRow row in tabs.Rows)
            {
                tabList.Items.Add(row["Table_Name"].ToString());
            }
        }

        private void cbxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (tabList.Items.Count == 0)
            {
                return;
            }
            for (int i = 0; i < tabList.Items.Count; i++)
            {
                tabList.SetItemChecked(i, cbxSelectAll.Checked);
            }
        }
        private void nameStyleBind()
        {
            Dictionary<int, string> nstype;
            nstype = new Dictionary<int, string>();
            nstype.Add(0, "原名称");
            nstype.Add(1, "大驼峰");
            nstype.Add(2, "小驼峰");
            BindingSource tbs = new BindingSource();
            tbs.DataSource = nstype;

            ccbTableNameStyle.DataSource = tbs;
            ccbTableNameStyle.DisplayMember = "Value";
            ccbTableNameStyle.ValueMember = "Key";
        }

        private void btnTmplSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = txtTmplPath.Text;
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                txtTmplPath.Text = fbd.SelectedPath;
            }
        }
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            string tmplDir = txtTmplPath.Text;
            if (tmplDir == "")
            {
                return;
            }
            string configFile = Path.GetFullPath(tmplDir + "/config.json");
            if (!File.Exists(configFile))
            {
                return;
            }
            string dbname = ccbDataBase.SelectedValue.ToString();
            if (dbname == "")
            {
                return;
            }
            if (tabList.CheckedItems.Count == 0)
            {
                return;
            }
            List<string> tabs = new List<string>();
            foreach (object o in tabList.CheckedItems)
            {
                tabs.Add(o.ToString());
            }
            string cfgJson = File.ReadAllText(configFile, System.Text.Encoding.UTF8);
            try
            {

                dcConfigs dccfg = JsonHepler.JsonParse<dcConfigs>(cfgJson);
                RunSettings sets = new RunSettings
                {
                    dbName = dbname,
                    dbHandle = dbHandle,
                    tabs = tabs,
                    tableNameStyle = (int)ccbTableNameStyle.SelectedValue,
                    tmplDir = tmplDir
                };
                DbCodeUtil dcUtil = new DbCodeUtil(dccfg, sets);
                dcUtil.StartGenerate();
                MessageBox.Show("完成", "提示");
            }
            catch(Exception ex)
            {
                MessageBox.Show("失败:"+ex.Message, "提示");
                return;
            }

        }
    }
}
