﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbCodeGenerator
{
    class RunSettings
    {
        public IDbHandle dbHandle { get; set; }
        public string dbName { get; set; }
        public List<string> tabs { get; set; }
        public string tmplLeft { get; set; }
        public string tmplRight { get; set; }
        public int tableNameStyle { get; set; }
        public int fieldNameStyle { set; get; }
        public string tmplDir { set; get; }
        public string getTmplStr(string str)
        {
            string l = this.tmplLeft.Replace("[", "\\[").Replace("{", "\\{").Replace("(", "\\(").Replace("<", "\\<");
            string r = this.tmplRight.Replace("]", "\\]").Replace("}", "\\}").Replace(")", "\\)").Replace(">", "\\>");
            return l + str + r;
        }
    }
}
