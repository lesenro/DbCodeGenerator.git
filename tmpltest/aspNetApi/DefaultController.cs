﻿@{
	var appcfg=Model.dcCfgs.appConfigs;
}using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using @(appcfg["appName"]).Models;

namespace @(appcfg["appName"]).Controllers
{
    public class @(Model.filename)Controller : ApiBaseController
    {
        [HttpGet]
        public IHttpActionResult Index()
        {
            var reqParams = InitRequestParams();
            var result = dbHelper.GetList<@(Model.filename)>(reqParams);
            return Ok(result);
        }
        [HttpGet]
        public IHttpActionResult GetOne(int id)
        {
            var result = dbHelper.GetById<@(Model.filename)>(id);
            return Ok(result);
        }
        [HttpPost]
        public IHttpActionResult Post(@(Model.filename) info)
        {
            var result = dbHelper.Add(info);
            return Ok(result);
        }
        [HttpPut]
        public IHttpActionResult Put(@(Model.filename) info)
        {
            var result = dbHelper.Edit(info.Id, info);
            return Ok(result);
        }
        [HttpDelete]
        public IHttpActionResult Delete(int[] ids)
        {
            var result = dbHelper.MultDelete<@(Model.filename)>(ids);
            return Ok(result);
        }
    }
}
