﻿@{
	var appcfg=Model.dcCfgs.appConfigs;
}using @(appcfg["appName"]).Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.SessionState;
using @(appcfg["appName"]).Utilities;

namespace @(appcfg["appName"]).Controllers
{
    public class ApiBaseController : ApiController
    {
        public HttpContext Context { get; set; }
        public HttpSessionState Session { get; set; }
        public HttpServerUtility Server { get; set; }
        public HttpRequest httpRequest { get; set; }
        public HttpResponse httpResponse { get; set; }
        public DataBaseHelper<DbCtx> dbHelper {get;set;}
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            dbHelper = new DataBaseHelper<DbCtx>(new DbCtx());
            base.Initialize(controllerContext);
            Context = HttpContext.Current;
            Session = Context.Session;
            Server = Context.Server;
            httpRequest = Context.Request;
            httpResponse = Context.Response;
        }
        public RequestParams InitRequestParams()
        {
            return new RequestParams()
            {
                columns = httpRequest.QueryString["columns"] ?? "",
                orderby = httpRequest.QueryString["orderby"] ?? "",
                query = httpRequest.QueryString["query"] ?? "",
                limit = Tools.ConvertType<int>(httpRequest.QueryString["limit"], 10),
                offset = Tools.ConvertType<int>(httpRequest.QueryString["offset"], 0),
                pagenum = Tools.ConvertType<int>(httpRequest.QueryString["pagenum"], 1)
            };
        }
    }
}
