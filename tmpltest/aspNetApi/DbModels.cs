﻿@{	var appcfg=@Model.dcCfgs.appConfigs;}
namespace @(appcfg["appName"]).Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    [Table("@(appcfg["DbName"]).@(Model.tabname)")]
    public partial class @(Model.filename)
    {
		@foreach(var col in Model.tabinfo){
			if(col.columnType=="text"){
			@Raw("\t\t[Column(TypeName = \"text\")]\r\n")
			@Raw("\t\t[StringLength(65535)]\r\n")
			@Raw("\t\tpublic string "+col.columnName+" { get; set; }\r\n\r\n")	
			}else if(col.columnType=="varchar"){
			@Raw("\t\t[StringLength("+col.size.ToString()+")]\r\n")
			@Raw("\t\tpublic string "+col.columnName+" { get; set; }\r\n\r\n")	
			}else if(col.columnType=="datetime"){
			@Raw("\t\tpublic DateTime "+col.columnName+" { get; set; }\r\n\r\n")	
			}else if(col.columnType=="bigint"){
			@Raw("\t\tpublic long "+col.columnName+" { get; set; }\r\n\r\n")	
			}else{
			@Raw("\t\tpublic "+col.columnType+" "+col.columnName+" { get; set; }\r\n\r\n")	
			}
		}
    }
}