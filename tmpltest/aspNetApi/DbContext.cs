@{
	var appcfg=Model.dcCfgs.appConfigs;
}namespace @(appcfg["appName"]).Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public partial class DbCtx : DbContext
    {
        public DbCtx()
            : base("name=DbCtx")
        {
        }
		@foreach(var item in Model.tables){
			var tabName= Model.codeTools.humpNaming(Model.runSet.tableNameStyle,item.key);
			@Raw("\t\tpublic virtual DbSet<"+tabName+"> "+tabName+" { get; set; }\r\n")
		}


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
			@foreach(var item in Model.tables){
				var tabName= Model.codeTools.humpNaming(Model.runSet.tableNameStyle,item.key);				
				foreach(var col in item.value){
					if(col.columnType=="text"||col.columnType=="varchar"){
					@Raw("\t\t\tmodelBuilder.Entity<"+tabName+">()\r\n")
					@Raw("\t\t\t\t.Property(e => e."+col.columnName+")\r\n")
					@Raw("\t\t\t\t.IsUnicode(false);\r\n")
					}
				}
				@Raw("\r\n")
			}
        }
    }
}
