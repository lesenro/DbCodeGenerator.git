﻿@{
	var appcfg=Model.dcCfgs.appConfigs;
}using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace @(appcfg["appName"]).Utilities
{
    public class ListResult
    {
        public IQueryable List { get; set; }
        public int Count { get; set; }
    }
}