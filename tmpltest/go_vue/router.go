@{
	var appcfg=Model.dcCfgs.appConfigs;
}
// @("@")APIVersion 1.0.0
// @("@")Title beego Test API
// @("@")Description beego has a very cool tools to autogenerate documents for your API
// @("@")Contact astaxie@gmail.com
// @("@")TermsOfServiceUrl http://beego.me/
// @("@")License Apache 2.0
// @("@")LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"lesenro/@(appcfg["appName"])/api/controllers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {
	ns := beego.NewNamespace("/api",
		beego.NSBefore(cors.Allow(&cors.Options{
			AllowCredentials: true,
			AllowHeaders:     []string{"content-type"},
			AllowOrigins:     []string{"*"},
			AllowMethods:     []string{"GET", "POST", "OPTIONS", "PATCH", "PUT", "DELETE"},
			// ExposeHeaders:    []string{"Content-Length"},
		}), controllers.LoginAuth),
		@foreach(var item in Model.tables){
			var ctrlName= Model.codeTools.humpNaming(1,item.key);
			<replace>
			beego.NSNamespace("/@item.key",
				beego.NSInclude(
					&controllers.@(ctrlName)Controller{},
				),
			),
			</replace>
		}
		
	)
	beego.AddNamespace(ns)
	beego.Router("/", &controllers.MainController{})
}
