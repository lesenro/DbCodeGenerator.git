
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
@foreach(var item in Model.tables){
    var funcName= Model.codeTools.humpNaming(2,item.key);
    <replace> import @(funcName)Module from './@(item.key)'; </replace>
}

const store = new Vuex.Store({
    modules: {
        @foreach(var item in Model.tables){
            var funcName= Model.codeTools.humpNaming(2,item.key);
        <replace> @(item.key): @(funcName)Module,  </replace>
        }
    }
})


export default store;