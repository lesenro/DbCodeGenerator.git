@{
	var index=1;
}
import {
	@foreach(var item in Model.tables){
		var mName= Model.codeTools.humpNaming(1,item.key);
	<replace> @(mName) ,  </replace>
	}
  } from './pages'
  ///////////////
[
@foreach(var item in Model.tables){
	var funcName= Model.codeTools.humpNaming(2,item.key);
	var mName= Model.codeTools.humpNaming(1,item.key);
<replace> 
{
	icon: 'el-icon-eleme',
	path: '@(item.key)',
	component: @(mName),
	name: "@(item.key)管理",
	powers: [
	  {
		name: "查询",
		keys: [
		  "menu_@(index++)",
		  "/@(item.key)/getbyid",
		  "/@(item.key)/getlist",
		],
		isMenu: true,
	  },
	  {
		name: "添加",
		keys: ["/@(item.key)/add"],
		isMenu: false,
	  },
	  {
		name: "修改",
		keys: ["/@(item.key)/update", "/@(item.key)/getbyid"],
		isMenu: false,
	  },
	  {
		name: "删除",
		keys: ["/@(item.key)/del"],
		isMenu: false,
	  }
	]
  },
  </replace>
}
]