@foreach(var item in Model.tables){
    var mName= Model.codeTools.humpNaming(1,item.key);
    var fName= Model.codeTools.humpNaming(Model.runSet.tableNameStyle,item.key);
<replace> export { default as @mName } from './@fName';  </replace>
}