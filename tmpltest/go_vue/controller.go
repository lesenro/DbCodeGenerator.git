@{	
	var appcfg=@Model.dcCfgs.appConfigs;
	var tabName =@Model.codeTools.humpNaming(1,Model.tabname);
}package controllers

import (
	"encoding/json"
	"errors"
	"lesenro/@(appcfg["appName"])/api/models"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
)

// @(tabName)Controller operations for @(tabName)
type @(tabName)Controller struct {
	beego.Controller
}

// URLMapping ...
func (c *@(tabName)Controller) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @("@")Title Post
// @("@")Description create @(tabName)
// @("@")Param	body		body 	models.@(tabName)	true		"body for @(tabName) content"
// @("@")Success 201 {int} models.@(tabName)
// @("@")Failure 403 body is empty
// @("@")router /add [post]
func (c *@(tabName)Controller) Post() {
	var v models.@(tabName)
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
		if _, err := models.Add@(tabName)(&v); err == nil {
			c.Ctx.Output.SetStatus(201)
			c.Data["json"] = models.Result{
				Code: 0,
				Msg:  "ok",
				Data: v,
			}
		} else {
			c.Data["json"] = models.Result{
				Code: 201,
				Msg:  err.Error(),
			}
		}
	} else {
		c.Data["json"] = models.Result{
			Code: 202,
			Msg:  err.Error(),
		}
	}
	c.ServeJSON()
}

// GetOne ...
// @("@")Title Get One
// @("@")Description get @(tabName) by id
// @("@")Param	id		query 	string	true		"The key for staticblock"
// @("@")Success 200 {object} models.@(tabName)
// @("@")Failure 403 id is empty
// @("@")router /getbyid [get]
func (c *@(tabName)Controller) GetOne() {
	idStr := c.GetString("id")
	id, _ := strconv.Atoi(idStr)
	v, err := models.Get@(tabName)ById(id)

	if err != nil {
		c.Data["json"] = models.Result{
			Code: 203,
			Msg:  err.Error(),
		}
	} else {
		c.Data["json"] = models.Result{
			Code: 0,
			Msg:  "ok",
			Data: v,
		}
	}
	c.ServeJSON()
}

// GetAll ...
// @("@")Title Get All
// @("@")Description get @(tabName)
// @("@")Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @("@")Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @("@")Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @("@")Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @("@")Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @("@")Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @("@")Success 200 {object} models.@(tabName)
// @("@")Failure 403
// @("@")router /getlist [get]
func (c *@(tabName)Controller) GetAll() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAll@(tabName)(query, fields, sortby, order, offset, limit)
	total, _ := models.GetCount@(tabName)(query)
	if err != nil {
		c.Data["json"] = models.Result{
			Code: 204,
			Msg:  err.Error(),
		}
	} else {
		c.Data["json"] = models.Result{
			Code: 0,
			Msg:  "ok",
			Data: map[string]interface{}{
				"total": total,
				"list":  l,
			},
		}

	}
	c.ServeJSON()
}

// Put ...
// @("@")Title Put
// @("@")Description update the @(tabName)
// @("@")Param	body		body 	models.@(tabName)	true		"body for @(tabName) content"
// @("@")Success 200 {object} models.@(tabName)
// @("@")Failure 403 :id is not int
// @("@")router /update [put]
func (c *@(tabName)Controller) Put() {
	v := models.@(tabName){}
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
		if err := models.Update@(tabName)ById(&v); err == nil {
			c.Data["json"] = models.Result{
				Code: 0,
				Msg:  "ok",
				Data: v,
			}
		} else {
			c.Data["json"] = models.Result{
				Code: 205,
				Msg:  err.Error(),
			}
		}
	} else {
		c.Data["json"] = models.Result{
			Code: 206,
			Msg:  err.Error(),
		}
	}
	c.ServeJSON()
}

// Delete ...
// @("@")Title Delete
// @("@")Description delete the @(tabName)
// @("@")Param	id		query 	string	true		"The id you want to delete"
// @("@")Success 200 {string} delete success!
// @("@")Failure 403 id is empty
// @("@")router /del [delete]
func (c *@(tabName)Controller) Delete() {
	idStr := c.GetString("id")
	id, _ := strconv.Atoi(idStr)
	if v, err := models.Delete@(tabName)(id); err == nil {
		c.Data["json"] = models.Result{
			Code: 0,
			Msg:  "ok",
			Data: v,
		}
	} else {
		c.Data["json"] = models.Result{
			Code: 207,
			Msg:  err.Error(),
		}
	}
	c.ServeJSON()
}


