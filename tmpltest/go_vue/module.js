@{	
	var appcfg=@Model.dcCfgs.appConfigs;
	var funcName =@Model.codeTools.humpNaming(2,Model.tabname);
}import {
    @(funcName)GetList,
    @(funcName)Add,
    @(funcName)Update,
    @(funcName)Del,
    @(funcName)GetById,
} from '../platformApi';

const @(funcName)Module = {
    namespaced: true,
    state: {
        list: [],
        loading: false,
    },
    mutations: {
        setLoading(state, val) {
            state.loading = val;
        },
    },
    actions: {
        async getList(ctx, params) {
            ctx.commit("setLoading", true);
            let result;
            try {
                result = await @(funcName)GetList(params.getQueryData());
            } finally {
                ctx.commit("setLoading", false);
            }
            return result;
        },
        async add(ctx, params) {
            let result = await @(funcName)Add(params);
            return result;
        },
        async getById(ctx, params) {
            let result = await @(funcName)GetById(params);
            return result;
        },
        async update(ctx, params) {
            let result = await @(funcName)Update(params);
            return result;
        },
        async del(ctx, params) {
            let result = await @(funcName)Del(params);
            return result;
        },
    }
}

export default @(funcName)Module;