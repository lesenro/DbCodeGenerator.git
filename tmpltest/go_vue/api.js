@{	
	var appcfg=@Model.dcCfgs.appConfigs;
}

@foreach(var item in Model.tables){
        var funcName = Model.codeTools.humpNaming(2,item.key);
        var tname= item.key;
<replace>

/**
 * @(tname)-分页列表
 * apiurl:/@(tname)/getlist
 */
export async function @(funcName)GetList(params) {
    if (!params.fields) {
        params.fields = "";
    }
    if (!params.sortby) {
        params.sortby = "Id";
        params.order = "desc";
    }

    const query = stringify(params);
    return request(`${apiUrl}/@(tname)/getlist?${query}`, {
        method: 'GET',
        body: {},
    });
}

/**
 * @(tname)-添加
 * apiurl:/@(tname)/add
 */
export async function @(funcName)Add(params) {
    const data = Object.assign({}, params);
    return request(`${apiUrl}/@(tname)/add`, {
        method: 'POST',
        body: {
            ...data,
        },
    });
}

/**
  * @(tname)-修改
  * apiurl:/@(tname)/update
  */
export async function @(funcName)Update(params) {
    const data = Object.assign({}, params);
    delete data.CreateTime;
    return request(`${apiUrl}/@(tname)/update`, {
        method: 'PUT',
        body: {
            ...data,
        },
    });
}

/**
  * @(tname)-根据id删除
  * apiurl:/@(tname)/del
  */
export async function @(funcName)Del(id) {
    return request(`${apiUrl}/@(tname)/del?id=${id}`, {
        method: 'DELETE',
        body: {},
    });
}
/**
  * @(tname)-根据id查询
  * apiurl:/@(tname)/getbyid
  */
export async function @(funcName)GetById(id) {
    return request(`${apiUrl}/@(tname)/getbyid?id=${id}`, {
        method: 'GET',
        body: {},
    });
}

</replace>
}