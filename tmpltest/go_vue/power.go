@foreach(var item in Model.tables){
<replace> {
	PowName:   "查询@(item.key)",
	PowDesc:   "查询@(item.key)",
	PowUrl:    "/@(item.key)/getlist",
	PowCode:   "/@(item.key)/getlist",
	IsDel:     0,
	PowType:   2,
	PowSelect: 0,
},
{
	PowName:   "根据id查询@(item.key)",
	PowDesc:   "根据id查询@(item.key)",
	PowUrl:    "/@(item.key)/getbyid",
	PowCode:   "/@(item.key)/getbyid",
	IsDel:     0,
	PowType:   2,
	PowSelect: 0,
},

{
	PowName:   "添加@(item.key)",
	PowDesc:   "添加@(item.key)",
	PowUrl:    "/@(item.key)/add",
	PowCode:   "/@(item.key)/add",
	IsDel:     0,
	PowType:   2,
	PowSelect: 0,
},
{
	PowName:   "更新@(item.key)",
	PowDesc:   "更新@(item.key)",
	PowUrl:    "/@(item.key)/update",
	PowCode:   "/@(item.key)/update",
	IsDel:     0,
	PowType:   2,
	PowSelect: 0,
},
{
	PowName:   "根据id删除@(item.key)",
	PowDesc:   "根据id删除@(item.key)",
	PowUrl:    "/@(item.key)/del",
	PowCode:   "/@(item.key)/del",
	IsDel:     0,
	PowType:   2,
	PowSelect: 0,
},
</replace>
}