@{
	var tabName= Model.codeTools.humpNaming(Model.runSet.tableNameStyle,Model.tabname);
}
import React from 'react';
import {
    Tabs,
    Form,
    Input,
    Icon,
    Button,
} from 'antd';
import {AppTools} from '../AppTools';
import {ApiService} from '../ApiService';
//设置操作
const Actions = {
    getInfo: ApiService.getOne@(tabName),
    addSave: ApiService.post@(tabName),
    editSave: ApiService.put@(tabName)
};
const ButtonGroup = Button.Group;
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 6
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
let history;
class EditForm extends React.Component {
    state = {
		@foreach(var col in Model.tabinfo){
			@Raw("\t\t"+col.columnName+":"+col.defaultValue+",\r\n")
		}		
    };
    saveSuccess(resp) {
        if (resp.code === "success") {
            history.goBack();
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
			if (!err) {
				var send = Object.assign({}, values);
				send.Id = this.state.Id;
				if (send.Id > 0) {
					Actions.editSave(send).then(this.saveSuccess);
				} else {
					Actions.addSave(send).then(this.saveSuccess);
				}
			}
		});
    }

    componentDidMount() {
        history = this.props.history;
        var params = AppTools.queryParams(this.props.location.search);
        if (params) {
            Actions.getInfo(params.id).then(resp => {
				if (resp.code === "success") {
					this.setState(resp.data);
				}
			});
        }
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        const operations = <ButtonGroup>
            <Button type="primary" onClick={this.handleSubmit}>
                <Icon type="save"/>
                保存
            </Button>
        </ButtonGroup>;
        let route = this.props.route;
        let title = <span><Icon type={route.props.icon || "edit"}/>{route.props.name}</span>;
        return <div>
            <Tabs tabBarExtraContent={operations}>
                <TabPane tab={title} key="1"/>
            </Tabs>
            <Form>
			@foreach(var col in Model.tabinfo){
                <FormItem {...formItemLayout} label="@col.columnName" hasFeedback>
                    {getFieldDecorator('@col.columnName', {
                        initialValue: this.state.@col.columnName,
                        rules: [
                            {
                                required: true,
                                message: 'plese input @col.columnName'
                            }
                        ]
                    })(<Input/>)}
                </FormItem>
			}
            </Form>
        </div>
    }
}
export const @(tabName)Edit = Form.create()(EditForm);
