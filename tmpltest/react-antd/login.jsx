import React from 'react';
import { Layout, Menu, Form, Card, notification } from 'antd';
import { LoginForm } from './LoginForm'
import { ApiService } from '../ApiService'
import { AppTools } from '../AppTools'

const { Header, Content, Footer } = Layout;
const AdminLoginForm = Form.create()(LoginForm);

export class LoginRoot extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: false,
            captchaUrl: ApiService.captchaUrl(),
        };
    }
    loginFrm = null;
    render() {
        return (
            <Layout className="login-layout">
                <Header>
                    <div className="container">
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={['2']}
                            style={{
                                lineHeight: '64px',
                                paddingLeft: "15px",
                                paddingRight: "15px"
                            }}
                            className="navbar-right">
                            <Menu.Item key="1">首页</Menu.Item>
                            <Menu.Item key="2">登录</Menu.Item>
                        </Menu>
                    </div>
                </Header>
                <Content className="login-wrap">
                    <div
                        className="container"
                        style={{
                            padding: 24,
                            minHeight: 280
                        }}>
                        <div className="row">
                            <div className="col-md-6">
                                <h1 className="c-white text-center">欢迎使用本系统</h1>
                                <div className="c-white text-center mg-v">登录之后更精彩</div>
                            </div>
                            <div className="col-md-6">
                                <Card
                                    title="管理登录"
                                    className="center-block"
                                    bordered={false}
                                    style={{
                                        width: 300
                                    }}>
                                    <AdminLoginForm
                                        captcha={this.state.captchaUrl}
                                        onSubmit={e => {
                                            ApiService
                                                .login(e)
                                                .then(resp => {
                                                    if (resp.code === "success") {
                                                        if(resp.data.UserType===0){
                                                            AppTools.redirect("/admin");
                                                            return;
                                                        }else{
                                                            notification.warning({ message: "无权访问", description: "您无权访问本系统!" });
                                                            ApiService.logout();
                                                        }
                                                    }
                                                    this.setState({captchaUrl:ApiService.captchaUrl()});
                                                });
                                        }}
                                        updateCaptcha={() => {
                                            this.setState({captchaUrl:ApiService.captchaUrl()});
                                        }} />
                                </Card>
                            </div>
                        </div>
                    </div>
                </Content>
                <Footer style={{
                    textAlign: 'center'
                }}>
                    Ant Design ©2016 Created by Ant UED
                </Footer>
            </Layout>
        );
    }
    componentDidMount() {
        window.document.title="管理登录";
        ApiService
            .testLogin()
            .then(resp => {
                if (resp.code === "success") {
                    if(resp.data.UserType===0){
                        notification.success({ message: "登录成功", description: "您已经登录成功!" });
                        AppTools.redirect("/admin/home");
                    }
                }
            });
    }
}
