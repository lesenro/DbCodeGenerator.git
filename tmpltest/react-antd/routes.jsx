import { LoginRoot, AdminCenter } from './component'
import React from 'react';
import { Redirect } from 'react-router-dom'
import { renderRoutes } from 'react-router-config';
import {@foreach(var item in Model.tables){
		var tabName= Model.codeTools.humpNaming(Model.runSet.tableNameStyle,item.key);
		@(tabName+"List,"+tabName+"Edit,")
	}
} from './admin';


function Home(props) {
  return <div className="Test">
    Home page
  </div>
}
export const routes = [{
    path: '/',
    exact: true,
    props: { title: 'home' },
    component: LoginRoot,
  }, {
    path: '/admin',
    component: AdminCenter,
	routes:[{
			path: '/admin',
			exact: true,
			component: () => <Redirect to="/admin/home" />
		}, {
			path: '/admin/home',
			props: {
				title: 'home',
				name: '管理首页',
				icon: 'home'
			},
			component: Home
		}, {
			path: '/admin/func',
			component: (props) => {
			return <div>
				{renderRoutes(props.route.routes)}
				</div>
			},
			props: {
			  name: '系统功能',
			  icon: 'appstore-o'
			},
			routes: [
			@foreach(var item in Model.tables){
				var tabName= Model.codeTools.humpNaming(Model.runSet.tableNameStyle,item.key);
				@("\t\t\t\t{\r\n")
				@Raw("\t\t\t\t\tpath: '/admin/func/"+tabName+"List',\r\n")
				@("\t\t\t\t\tprops: {\r\n")
				@Raw("\t\t\t\t\t\ttitle: '"+tabName+" list',\r\n")
				@Raw("\t\t\t\t\t\tname: '"+tabName+" list',\r\n")
				@Raw("\t\t\t\t\t\tinclude: ['/admin/func/"+tabName+"Edit']\r\n")
				@("\t\t\t\t\t},\r\n")
				@("\t\t\t\t\tcomponent: "+tabName+"List\r\n")
				@("\t\t\t\t}, {\r\n")
				@Raw("\t\t\t\t\tpath: '/admin/func/"+tabName+"Edit',\r\n")
				@("\t\t\t\t\tprops: {\r\n")
				@Raw("\t\t\t\t\t\ttitle: '"+tabName+" edit',\r\n")
				@Raw("\t\t\t\t\t\tname: '"+tabName+" edit',\r\n")
				@("\t\t\t\t\t\thidden: true\r\n")
				@("\t\t\t\t\t},\r\n")
				@("\t\t\t\t\tcomponent: "+tabName+"Edit\r\n")
				@("\t\t\t\t},\r\n")
			  }
			]
		  }
		]
  },];