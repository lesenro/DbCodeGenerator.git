@{
	var tabName= Model.codeTools.humpNaming(Model.runSet.tableNameStyle,Model.tabname);
}
import React from 'react';
import { Modal, message, Table, Form, Input, Tabs, Button, Icon } from 'antd';
import { AppTools } from '../AppTools';
import { ApiService, pageSize } from '../ApiService'
const FormItem = Form.Item;

const ButtonGroup = Button.Group;
const TabPane = Tabs.TabPane;
///设置操作
const Actions = {
    add: "/admin/func/@(tabName)Edit",
    list: "/admin/func/@(tabName)List",
    getList: ApiService.get@(tabName),
    deleteRec: ApiService.delete@(tabName),
};
//设置表列Id,BrandName,PinYin,IsHot
const columns = [
@foreach(var col in Model.tabinfo){
	@("{\r\n")
	@Raw("\ttitle:'"+col.columnName+"',\r\n")
	@Raw("\tdataIndex:'"+col.columnName+"'\r\n")
	@("},\r\n")
}
{
    title: '操作', dataIndex: '', width: 100, key: 'x', render: (text, record, index) =>
        <Button onClick={e => {
            e.preventDefault();
            AppTools.redirect(Actions.add + "?id=" + record.Id);
        }} type="primary" icon="edit">编辑</Button>
}];
//筛选
function getSearch() {
    if (queryString.trim() === "") {
        return "";
    }
    return "Title.contains:" + queryString+";GroupKey.contains:" + queryString;
}
class SearchPanel extends React.Component {
    state = {
        searchText: queryString,
    }
    render() {
        return <div>
            <div className="row">
                <div className="col-md-6">
                    <FormItem>
                        <Input prefix={<Icon type="question-circle-o" />} onChange={e => {
                            e.preventDefault();
                            this.setState({ searchText: e.target.value });
                        }} placeholder="搜索内容" value={this.state.searchText} />
                    </FormItem>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12 text-center" >
                    <Button type="primary" icon="search" onClick={e => {
                        e.preventDefault();
                        if (this.props.onSearch) {
                            this.props.onSearch(this.state.searchText);
                        }
                    }}>搜索</Button>{" "}
                    <Button type="danger" icon="reload" onClick={e => {
                        e.preventDefault();
                        this.setState({ searchText: "" });
                        if (this.props.onClear) {
                            this.props.onClear();
                        }
                    }}>清除</Button>
                </div>
            </div>
        </div>
    }
}
let selectedRows = [];
let pageNum = 0;
let count = 0;
let queryString = "";
export class @(tabName)List extends React.Component {
    openMenus = [];
    state = {
        collapsed: true,
        dataList: [],
    };
    pageDataLoad(pn, query = "") {
        pageNum = pn;
        queryString = query;
        Actions.getList(pn, getSearch(query)).then(resp => {
            if (resp.code === "success") {
                var list = [];
                var brandIds = [];
                if (resp.data.List.length > 0) {
                    resp.data.List.forEach(function (item) {
                        item.key = item.Id;
                        list.push(item);
                        brandIds.push(item.BrandId);
                    }, this);
                }
                count = resp.data.Count;
                this.setState({ "dataList": list });
            }
        });
    }
    render() {
        const rowSelection = {
            onChange: (selectedRowKeys, srows) => {
                selectedRows = srows;
            },
        };
        const operations = <ButtonGroup>
            <Button
                type="primary"
                onClick={e => {
                    e.preventDefault();
                    AppTools.redirect(Actions.add);
                }}>
                <Icon type="plus" />
                添加
            </Button>
            <Button
                type="danger"
                onClick={e => {
                    e.preventDefault();
                    if (selectedRows.length === 0) {
                        message.warning('请先选中要删除的记录');
                        return;
                    }
                    var self = this;
                    Modal.confirm({
                        title: '删除确认',
                        content: '确认要删除 ' + selectedRows.length.toString() + ' 条记录吗?',
                        onOk() {
                            var ids = [];
                            selectedRows.forEach(function (item) {
                                ids.push(item.Id);
                            }, this);
                            Actions.deleteRec(ids).then(resp => {
                                if (resp.code === "success") {
                                    self.pageDataLoad(pageNum);
                                    message.success(resp.data + ' 条记录,成功删除');
                                }
                            });
                        },
                        onCancel() { },
                    });
                }}>
                <Icon type="delete" />
                删除
            </Button>
            <Button type={this.state.collapsed ? "" : "primary"} onClick={e => {
                this.setState({ collapsed: !this.state.collapsed });
            }}>
                筛选 <Icon type={this.state.collapsed ? "down" : "up"} />
            </Button>
        </ButtonGroup>;
        let pagination = {
            pageSize: pageSize,
            current: pageNum,
            defaultCurrent: pageNum,
            total: count,
            size: "default",
            onChange: (e) => {
                AppTools.redirect(Actions.list + "?pn=" + e + "&query=" + queryString);
                this.pageDataLoad(e);
            }
        };
        let footer = count > 0 ? "每页" + pageSize.toString() + "条 共" + count.toString() + "条 " + pageNum.toString() + "/" + (Math.ceil(count / pageSize)).toString() + "页" : "";
        let route = this.props.route;
        let title = <span><Icon type={route.props.icon || "desktop"} />{route.props.name}</span>;
        return <div>
            <Tabs tabBarExtraContent={operations}>
                <TabPane tab={title} key="1" />
            </Tabs>
            {!this.state.collapsed &&
                <div className="panel panel-default">
                    <div className="panel-body">
                        <SearchPanel onSearch={e => {
                            AppTools.redirect(Actions.list + "?pn=1&query=" + escape(e));
                            this.pageDataLoad(1, e);
                        }} onClear={() => {
                            AppTools.redirect(Actions.list + "?pn=1");
                            this.pageDataLoad(1, "");
                        }} />
                    </div>
                </div>
            }
            <Table size="small" pagination={pagination} rowSelection={rowSelection} columns={columns} dataSource={this.state.dataList} bordered footer={() => footer} />
        </div>
    }
    componentDidMount() {

        var pn = 1;
        var query = "";
        var params = AppTools.queryParams(this.props.location.search);
        if (params && params.pn) {
            pn = parseInt(params.pn, 10);
            query = params.query ? unescape(params.query) : "";
        }
        this.pageDataLoad(pn, query);
    }
}