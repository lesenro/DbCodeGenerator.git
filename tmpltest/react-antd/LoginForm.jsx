import React from 'react';
import {
    Form,
    Input,
    Icon,
    Button,
    Col,
    Tooltip
} from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;

export class LoginForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this
            .props
            .form
            .validateFields((err, values) => {
                if (!err) {
                    if (this.props.onSubmit) {
                        this.props.onSubmit(values);
                    }
                }
            });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="login-form">
                <FormItem>
                    {getFieldDecorator('username', {
                        rules: [
                            {
                                required: true,
                                min: 5,
                                max: 24,
                                message: '请输入正确的用户名!'
                            }
                        ]
                    })(
                        <Input prefix={< Icon type="user" />} placeholder="用户帐号" />
                        )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('password', {
                        rules: [
                            {
                                required: true,
                                min: 5,
                                max: 24,
                                message: '请输入正确的密码!'
                            }
                        ]
                    })(
                        <Input prefix={< Icon type="lock" />} type="password" placeholder="用户密码" />
                        )}
                </FormItem>
                <FormItem>

                    <InputGroup size="large">
                        <Col span={12}>
                            {getFieldDecorator('captcha', {
                                rules: [
                                    {
                                        required: true,
                                        len: 4,
                                        message: '请输入验证码!'
                                    }
                                ]
                            })(<Input placeholder="验证码" />)}
                        </Col>
                        <Col span={12} className="text-right">
                            <Tooltip title="看不清? 换一张">
                                <img
                                    src={this.props.captcha}
                                    alt=""
                                    style={{
                                        height: "32px"
                                    }}
                                    className="captcha"
                                    onClick={(e) => {
                                        if (this.props.updateCaptcha) {
                                            this.props.updateCaptcha();
                                        }
                                    }} />
                            </Tooltip>
                        </Col>
                    </InputGroup>
                </FormItem>
                <FormItem className="text-center">
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        登录
                    </Button>
                </FormItem>
            </Form>
        );
    }

}