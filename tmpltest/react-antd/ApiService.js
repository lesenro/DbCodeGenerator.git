import axios from 'axios';
import { AppTools } from './AppTools';
import { message } from 'antd';

import md5 from 'blueimp-md5';

const CookieNames={
	USER_ONLINE:"USER_ONLINE",
};
axios.defaults.withCredentials = true;
let useronline = AppTools.getJsonCookie(CookieNames.USER_ONLINE);
if (useronline) {
    axios.defaults.headers.common['Authorization'] = useronline.UserId + ":" + useronline.AccessToken;
}
export const pageSize = 10;
class SearchParams {
    columns = "";
    limit = pageSize;
    offset = 0;
    pagenum = 1;
    order = "desc";
    sortby = "Id";
    query = "";
    computational() {
        this.offset = (this.pagenum - 1) * this.limit;
    }
}
///
const ajax = function (config, showErr = true, redirect = true) {
    return axios(config).then(resp => {
        return { code: "success", data: resp.data }
    }).catch(error => {
        if (showErr && error.response && error.response.data) {
            message.error(error.response.data.Message);
        }
        //无权限时跳转到登录页
        if (redirect && error.response && error.response.status === 401) {
            AppTools.redirect("/");
        }
        //处理错误
        return { code: "error", data: error.response };
    });
}
const api_server = "/";
export class ApiService {
    //获取用户登录信息
    static getLoginInfo() {
        return AppTools.getJsonCookie(CookieNames.USER_ONLINE);
    };
    static testLogin() {
        return ajax({
            url: api_server + "api/member",
            method: "get"
        }, false, false).then(resp => {
            if (resp.code === "success") {
                AppTools.setJsonCookie(CookieNames.USER_ONLINE, resp.data);
                useronline = resp.data;
                axios.defaults.headers.common['Authorization'] = resp.data.UserId + ":" + resp.data.AccessToken;
            }
            return resp;
        });
    };
    static login(data) {
        var send = Object.assign({}, data);
        send.password = md5(md5(send.password) + send.captcha);
        return ajax({
            url: api_server + "api/member/login",
            method: "post",
            data: send
        }).then(resp => {
            if (resp.code === "success") {
                AppTools.setJsonCookie(CookieNames.USER_ONLINE, resp.data);
                useronline = resp.data;
                axios.defaults.headers.common['Authorization'] = resp.data.UserId + ":" + resp.data.AccessToken;
            }
            return resp;
        });
    };
    static logout() {
        return ajax({
            url: api_server + "api/member/logout",
            method: "get"
        }).then(resp => {
            if (resp.code === "success") {
                AppTools.setJsonCookie(CookieNames.USER_ONLINE, null);
                useronline = null;
                delete axios.defaults.headers.common['Authorization'];
            }
            return resp;
        });
    };
    static captchaUrl() {
        return api_server + "captcha?" + AppTools.randCode();
    };
    //文件上传
    static getFileUpload() {
        return {
            uploadUrl: api_server + "api/ApiCommon/fileupload",
            Authorization: useronline.UserId + ":" + useronline.AccessToken
        };
    };
    //文件上传
    static fileUpload(file) {
        let formData = new FormData();
        formData.append('file', file);
        return ajax({
            url: api_server + "api/ApiCommon/fileupload",
            method: "post",
            data: formData
        });
    };

	@foreach(var item in Model.tables){
		var tabName= Model.codeTools.humpNaming(Model.runSet.tableNameStyle,item.key);

		@("\t//get "+tabName+" data list\r\n")
		@Raw("\tstatic get"+tabName+"(pn, query = \"\") {\r\n")
		@("\t\tvar param = new SearchParams();\r\n")
		@("\t\tparam.pagenum = pn;\r\n")
		@("\t\tparam.query = query;\r\n")
		@Raw("\t\tparam.columns = \"")
		var i=0;
		foreach(var col in item.value){
			if(i>0){
				@(",")
			}
			@(Model.codeTools.humpNaming(1,col.columnName))
			i++;
		}
		@Raw("\";\r\n")
		@("\t\tparam.computational();\r\n")
		@("\t\treturn ajax({\r\n")
		@Raw("\t\t\turl: api_server + \"api/"+tabName+"\",\r\n")
		@Raw("\t\t\tmethod: \"get\",\r\n")
		@("\t\t\tparams: param\r\n")
		@("\t\t});\r\n")
		@("\t}\r\n")
		
		@("\t//get "+tabName+" one data by id\r\n")
		@("\tstatic getOne"+tabName+"(id) {\r\n")
		@("\t\treturn ajax({\r\n")
		@Raw("\t\t\turl: api_server + \"api/"+tabName+"/GetOne/\" + id,\r\n")
		@Raw("\t\t\tmethod: \"get\"\r\n")
		@("\t\t});\r\n")
		@("\t};\r\n")
		
		
		@("\t//edit "+tabName+" data\r\n")
		@("\tstatic put"+tabName+"(data) {\r\n")
        @("\t\treturn ajax({\r\n")
        @Raw("\t\t\turl: api_server + \"api/"+tabName+"/Put\",\r\n")
        @Raw("\t\t\tmethod: \"put\",\r\n")
        @("\t\t\tdata: data\r\n")
        @("\t\t});\r\n")
		@("\t};\r\n")
		
		@("\t//add "+tabName+" data\r\n")
		@("\tstatic post"+tabName+"(data) {\r\n")
        @("\t\treturn ajax({\r\n")
        @Raw("\t\t\turl: api_server + \"api/"+tabName+"/Post\",\r\n")
        @Raw("\t\t\tmethod: \"post\",\r\n")
        @("\t\t\tdata: data\r\n")
        @("\t\t});\r\n")
		@("\t};\r\n")
		
		@("\t//delete "+tabName+" data\r\n")
		@("\tstatic delete"+tabName+"(ids) {\r\n")
        @("\t\treturn ajax({\r\n")
        @Raw("\t\t\turl: api_server + \"api/"+tabName+"/Delete\",\r\n")
        @Raw("\t\t\tmethod: \"delete\",\r\n")
        @("\t\t\tdata: ids\r\n")
        @("\t\t});\r\n")
		@("\t};\r\n")
    }
}