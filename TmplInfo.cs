﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace DbCodeGenerator
{
    [DataContract]
    class TmplInfo
    {
        [DataMember]
        public string tmplFile { get; set; }
        [DataMember]
        public string outlFile { get; set; }
        [DataMember]
        public string outPath { get; set; }
        [DataMember]
        public bool runOne { get; set; }
        [DataMember]
        public bool onlyCopy { get; set; } = false;
    }
    [DataContract]
    class dbTypeMap
    {
        [DataMember]
        public string dbtype { get; set; }
        [DataMember]
        public string totype { get; set; }
        [DataMember]
        public bool showSize { get; set; }
        [DataMember]
        public string defaultValue { get; set; }
    }
    [DataContract]
    class dcConfigs
    {
        [DataMember]
        public Dictionary<string, string> appConfigs { set; get; } = new Dictionary<string, string>();
        [DataMember]
        public List<dbTypeMap> typeMapped { get; set; }
        [DataMember]
        public List<TmplInfo> tmplList  { get; set; }
    }
}
