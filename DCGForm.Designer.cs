﻿namespace DbCodeGenerator
{
    partial class DCGForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.ccbDbType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ccbDataBase = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabList = new System.Windows.Forms.CheckedListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.cbxSelectAll = new System.Windows.Forms.CheckBox();
            this.btnDbConn = new System.Windows.Forms.Button();
            this.txtTmplPath = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnTmplSelect = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.txtServerAddress = new System.Windows.Forms.TextBox();
            this.numPort = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.ccbTableNameStyle = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).BeginInit();
            this.SuspendLayout();
            // 
            // ccbDbType
            // 
            this.ccbDbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ccbDbType.FormattingEnabled = true;
            this.ccbDbType.Location = new System.Drawing.Point(84, 11);
            this.ccbDbType.Name = "ccbDbType";
            this.ccbDbType.Size = new System.Drawing.Size(164, 20);
            this.ccbDbType.TabIndex = 0;
            this.ccbDbType.SelectedIndexChanged += new System.EventHandler(this.ccbDbType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "数据库类型";
            // 
            // ccbDataBase
            // 
            this.ccbDataBase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ccbDataBase.FormattingEnabled = true;
            this.ccbDataBase.Location = new System.Drawing.Point(84, 177);
            this.ccbDataBase.Name = "ccbDataBase";
            this.ccbDataBase.Size = new System.Drawing.Size(164, 20);
            this.ccbDataBase.TabIndex = 0;
            this.ccbDataBase.SelectedIndexChanged += new System.EventHandler(this.ccbDataBase_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 1;
            this.label6.Text = "数据库选择";
            // 
            // tabList
            // 
            this.tabList.CheckOnClick = true;
            this.tabList.FormattingEnabled = true;
            this.tabList.Location = new System.Drawing.Point(325, 11);
            this.tabList.Name = "tabList";
            this.tabList.Size = new System.Drawing.Size(199, 212);
            this.tabList.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(269, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "选择表";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(325, 232);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(199, 23);
            this.btnGenerate.TabIndex = 7;
            this.btnGenerate.Text = "开始生成";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // cbxSelectAll
            // 
            this.cbxSelectAll.AutoSize = true;
            this.cbxSelectAll.Location = new System.Drawing.Point(271, 29);
            this.cbxSelectAll.Name = "cbxSelectAll";
            this.cbxSelectAll.Size = new System.Drawing.Size(48, 16);
            this.cbxSelectAll.TabIndex = 8;
            this.cbxSelectAll.Text = "全选";
            this.cbxSelectAll.UseVisualStyleBackColor = true;
            this.cbxSelectAll.CheckedChanged += new System.EventHandler(this.cbxSelectAll_CheckedChanged);
            // 
            // btnDbConn
            // 
            this.btnDbConn.Location = new System.Drawing.Point(84, 148);
            this.btnDbConn.Name = "btnDbConn";
            this.btnDbConn.Size = new System.Drawing.Size(164, 23);
            this.btnDbConn.TabIndex = 7;
            this.btnDbConn.Text = "连接数据库";
            this.btnDbConn.UseVisualStyleBackColor = true;
            this.btnDbConn.Click += new System.EventHandler(this.btnDbConn_Click);
            // 
            // txtTmplPath
            // 
            this.txtTmplPath.Location = new System.Drawing.Point(84, 203);
            this.txtTmplPath.MaxLength = 255;
            this.txtTmplPath.Name = "txtTmplPath";
            this.txtTmplPath.Size = new System.Drawing.Size(123, 21);
            this.txtTmplPath.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 3;
            this.label8.Text = "模板路径";
            // 
            // btnTmplSelect
            // 
            this.btnTmplSelect.Location = new System.Drawing.Point(213, 203);
            this.btnTmplSelect.Name = "btnTmplSelect";
            this.btnTmplSelect.Size = new System.Drawing.Size(35, 21);
            this.btnTmplSelect.TabIndex = 9;
            this.btnTmplSelect.Text = "...";
            this.btnTmplSelect.UseVisualStyleBackColor = true;
            this.btnTmplSelect.Click += new System.EventHandler(this.btnTmplSelect_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.numPort);
            this.panel1.Controls.Add(this.txtServerAddress);
            this.panel1.Controls.Add(this.txtUserId);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnTmplSelect);
            this.panel1.Controls.Add(this.ccbDbType);
            this.panel1.Controls.Add(this.cbxSelectAll);
            this.panel1.Controls.Add(this.ccbTableNameStyle);
            this.panel1.Controls.Add(this.ccbDataBase);
            this.panel1.Controls.Add(this.btnDbConn);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnGenerate);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.tabList);
            this.panel1.Controls.Add(this.txtTmplPath);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(552, 277);
            this.panel1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "服务器地址";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "帐号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "密码";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "端口号";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(85, 93);
            this.txtPassword.MaxLength = 255;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(163, 21);
            this.txtPassword.TabIndex = 10;
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(85, 66);
            this.txtUserId.MaxLength = 255;
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(163, 21);
            this.txtUserId.TabIndex = 11;
            // 
            // txtServerAddress
            // 
            this.txtServerAddress.Location = new System.Drawing.Point(85, 39);
            this.txtServerAddress.MaxLength = 255;
            this.txtServerAddress.Name = "txtServerAddress";
            this.txtServerAddress.Size = new System.Drawing.Size(163, 21);
            this.txtServerAddress.TabIndex = 12;
            // 
            // numPort
            // 
            this.numPort.Location = new System.Drawing.Point(85, 120);
            this.numPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPort.Name = "numPort";
            this.numPort.Size = new System.Drawing.Size(162, 21);
            this.numPort.TabIndex = 13;
            this.numPort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 237);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 3;
            this.label9.Text = "文件名风格";
            // 
            // ccbTableNameStyle
            // 
            this.ccbTableNameStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ccbTableNameStyle.FormattingEnabled = true;
            this.ccbTableNameStyle.Location = new System.Drawing.Point(83, 234);
            this.ccbTableNameStyle.Name = "ccbTableNameStyle";
            this.ccbTableNameStyle.Size = new System.Drawing.Size(164, 20);
            this.ccbTableNameStyle.TabIndex = 0;
            // 
            // DCGForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 277);
            this.Controls.Add(this.panel1);
            this.Name = "DCGForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "数据库代码生成器";
            this.Load += new System.EventHandler(this.DCGForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox ccbDbType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ccbDataBase;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckedListBox tabList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.CheckBox cbxSelectAll;
        private System.Windows.Forms.Button btnDbConn;
        private System.Windows.Forms.TextBox txtTmplPath;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnTmplSelect;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown numPort;
        private System.Windows.Forms.TextBox txtServerAddress;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ccbTableNameStyle;
        private System.Windows.Forms.Label label9;
    }
}

